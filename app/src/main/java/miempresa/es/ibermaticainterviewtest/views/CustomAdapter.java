package miempresa.es.ibermaticainterviewtest.views;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import miempresa.es.ibermaticainterviewtest.R;
import miempresa.es.ibermaticainterviewtest.pojo.Pokemon;
import miempresa.es.ibermaticainterviewtest.pojo.responses.AllPokemonsResponse;

/**
 * Created by nico95k on 26/10/2019.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder>{

    List<Pokemon> pokemonList;

    List<Pokemon> pokemonListFiltered;

    Context context;

    public CustomAdapter(Context context, AllPokemonsResponse allPokemonsResponse) {
        this.context = context;
        this.pokemonListFiltered = allPokemonsResponse.getResults();
        this.pokemonList = allPokemonsResponse.getResults();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public View customView;
        public TextView pokemonTextView;
        public CustomViewHolder(View customView) {
            super(customView);
            this.customView = customView;
            this.pokemonTextView = this.customView.findViewById(R.id.pokemonName);
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_row, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.pokemonTextView.setText(pokemonListFiltered.get(position).getName());
        holder.customView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context,PokemonDetailActivity.class);
                i.putExtra("name",pokemonListFiltered.get(position).getName());
                pokemonListFiltered.get(position).buildId();
                i.putExtra("id",pokemonListFiltered.get(position).getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pokemonListFiltered.size();
    }


    public void filter(String text) {
        if (!text.isEmpty()){
            List<Pokemon> pokemonListFiltered = new ArrayList<>();
            for (Pokemon pokemon : pokemonList) {
                if(pokemon.getName().contains(text)) {
                    pokemonListFiltered.add(pokemon);
                }
            }
            this.pokemonListFiltered = pokemonListFiltered;
        }else {
            pokemonListFiltered = pokemonList;
        }
        notifyDataSetChanged();
    }


}
