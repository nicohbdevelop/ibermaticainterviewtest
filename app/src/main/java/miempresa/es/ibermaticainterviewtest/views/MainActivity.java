package miempresa.es.ibermaticainterviewtest.views;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import miempresa.es.ibermaticainterviewtest.ApiClient;
import miempresa.es.ibermaticainterviewtest.PokemonAPI;
import miempresa.es.ibermaticainterviewtest.R;
import miempresa.es.ibermaticainterviewtest.pojo.responses.AllPokemonsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Toolbar toolbar;
    RecyclerView recyclerView;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);
        toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        PokemonAPI api = ApiClient.getRetrofitInstance().create(PokemonAPI.class);
        Call<AllPokemonsResponse> call = api.getAllPokemon();
        call.enqueue(new Callback<AllPokemonsResponse>() {
            @Override
            public void onResponse(Call<AllPokemonsResponse> call, Response<AllPokemonsResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()){
                    Toast.makeText(MainActivity.this,"success",Toast.LENGTH_LONG).show();
                    buildPokemonList(response.body());
                }else{
                    Toast.makeText(MainActivity.this,"api call error: "+response.errorBody(),Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<AllPokemonsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview_toolbar, menu);

        MenuItem searcher = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) searcher.getActionView();
        searchView.setQueryHint("Search");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void buildPokemonList(AllPokemonsResponse allPokemonsResponse) {
        adapter = new CustomAdapter(this, allPokemonsResponse);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

}
