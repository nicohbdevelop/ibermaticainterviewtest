package miempresa.es.ibermaticainterviewtest.views;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import miempresa.es.ibermaticainterviewtest.ApiClient;
import miempresa.es.ibermaticainterviewtest.PokemonAPI;
import miempresa.es.ibermaticainterviewtest.R;
import miempresa.es.ibermaticainterviewtest.pojo.Ability;
import miempresa.es.ibermaticainterviewtest.pojo.GameIndex;
import miempresa.es.ibermaticainterviewtest.pojo.Move;
import miempresa.es.ibermaticainterviewtest.pojo.responses.PokemonDetailResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonDetailActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Toolbar toolbar;
    LinearLayout pokemonLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detail);

        String pokemonName = getIntent().getStringExtra("name");
        String pokemonId = getIntent().getStringExtra("id");

        pokemonLinear = findViewById(R.id.pokemonLinear);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(pokemonName);
        }

        progressDialog = new ProgressDialog(PokemonDetailActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        PokemonAPI api = ApiClient.getRetrofitInstance().create(PokemonAPI.class);
        Call<PokemonDetailResponse> call = api.getPokemonDetail(pokemonId);
        call.enqueue(new Callback<PokemonDetailResponse>() {
            @Override
            public void onResponse(Call<PokemonDetailResponse> call, Response<PokemonDetailResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()){
                    Toast.makeText(PokemonDetailActivity.this,"success",Toast.LENGTH_LONG).show();
                    PokemonDetailResponse result = response.body();
                    setData(result);
                }else{
                    Toast.makeText(PokemonDetailActivity.this,"API not working: " + response.errorBody(),Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<PokemonDetailResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PokemonDetailActivity.this, "error: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setData(PokemonDetailResponse pokemonDetailResponse) {
        List<Ability> abilityList = pokemonDetailResponse.getAbilities();

        LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        Typeface titlesFace = Typeface.createFromAsset(getAssets(),
                "font/montserrat_bold.ttf");

        // ability
        TextView title = new TextView(this);
        title.setText(getString(R.string.ability_title));
        title.setTextSize(24);
        title.setPadding(25,25,25,25);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(getColor(R.color.textColor));
        title.setTypeface(titlesFace);
        pokemonLinear.addView(title);

        for(Ability ability: abilityList) {
            TextView textview = new TextView(this);
            textview.setLayoutParams(layoutparams);
            textview.setText(ability.getAbility().getName());
            textview.setPadding(25,15,25,15);
            textview.setGravity(Gravity.CENTER);
            textview.setTextColor(getColor(R.color.textColor));
            pokemonLinear.addView(textview);
        }

        // base_experience
        int baseExperience = pokemonDetailResponse.getBaseExperience();

        TextView experienceTitle = new TextView(this);
        experienceTitle.setText(getString(R.string.experience_title));
        experienceTitle.setTextSize(24);
        experienceTitle.setPadding(25,25,25,25);
        experienceTitle.setGravity(Gravity.CENTER);
        experienceTitle.setTextColor(getColor(R.color.textColor));
        experienceTitle.setTypeface(titlesFace);
        pokemonLinear.addView(experienceTitle);

        TextView textview = new TextView(this);
        textview.setLayoutParams(layoutparams);
        textview.setText(String.valueOf(baseExperience));
        textview.setPadding(25,15,25,15);
        textview.setGravity(Gravity.CENTER);
        textview.setTextColor(getColor(R.color.textColor));
        pokemonLinear.addView(textview);

        // moves
        TextView movesTitle = new TextView(this);
        movesTitle.setText(getString(R.string.moves_title));
        movesTitle.setTextSize(24);
        movesTitle.setPadding(25,25,25,25);
        movesTitle.setGravity(Gravity.CENTER);
        movesTitle.setTextColor(getColor(R.color.textColor));
        movesTitle.setTypeface(titlesFace);
        pokemonLinear.addView(movesTitle);

        List<Move> moves = pokemonDetailResponse.getMoves();
        for(Move move: moves) {
            TextView moveTextview = new TextView(this);
            moveTextview.setLayoutParams(layoutparams);
            moveTextview.setText(move.getMove().getName());
            moveTextview.setPadding(25,15,25,15);
            moveTextview.setGravity(Gravity.CENTER);
            moveTextview.setTextColor(getColor(R.color.textColor));
            pokemonLinear.addView(moveTextview);
        }

        // gameIndex
        TextView gameIndexTitle = new TextView(this);
        gameIndexTitle.setText(getString(R.string.game_index));
        gameIndexTitle.setTextSize(24);
        gameIndexTitle.setPadding(25,25,25,25);
        gameIndexTitle.setGravity(Gravity.CENTER);
        gameIndexTitle.setTextColor(getColor(R.color.textColor));
        gameIndexTitle.setTypeface(titlesFace);

        pokemonLinear.addView(gameIndexTitle);

        List<GameIndex> gameIndexes = pokemonDetailResponse.getGameIndices();
        for(GameIndex gameIndex: gameIndexes) {
            TextView gameIndexTextview = new TextView(this);
            gameIndexTextview.setLayoutParams(layoutparams);
            gameIndexTextview.setText("Game Index: " + gameIndex.getGameIndex() + " Version: " + gameIndex.getVersion().getName());
            gameIndexTextview.setPadding(25,15,25,15);
            gameIndexTextview.setGravity(Gravity.CENTER);
            gameIndexTextview.setTextColor(getColor(R.color.textColor));
            pokemonLinear.addView(gameIndexTextview);
        }

    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
