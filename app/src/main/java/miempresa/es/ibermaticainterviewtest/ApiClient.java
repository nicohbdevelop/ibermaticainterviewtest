package miempresa.es.ibermaticainterviewtest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nico95k on 25/10/2019.
 */

public class ApiClient {

    private static Retrofit retrofit;

    private static final String BASE_URL = "https://pokeapi.co/api/v2/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
