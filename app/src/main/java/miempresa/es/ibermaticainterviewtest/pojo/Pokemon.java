package miempresa.es.ibermaticainterviewtest.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nico95k on 25/10/2019.
 */

public class Pokemon {
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("url")
    private String url;

    public void buildId() {
        String segments[] = url.split("/");
        id = segments[segments.length - 1];
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
