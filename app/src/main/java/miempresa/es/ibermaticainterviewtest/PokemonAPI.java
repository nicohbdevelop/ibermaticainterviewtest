package miempresa.es.ibermaticainterviewtest;

import miempresa.es.ibermaticainterviewtest.pojo.responses.PokemonDetailResponse;
import miempresa.es.ibermaticainterviewtest.pojo.responses.AllPokemonsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by nico95k on 25/10/2019.
 */

public interface PokemonAPI {

    @GET("pokemon?limit=200")
    Call<AllPokemonsResponse> getAllPokemon();

    @GET("pokemon/{id}")
    Call<PokemonDetailResponse> getPokemonDetail(@Path("id")String id);

}
